//
//  MonBeaconTrackerTVC.h
//  MonBeacon
//
//  Created by Matthew Rowland on 4/20/14.
//  Copyright (c) 2014 Matthew Rowland. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "MonBeaconUUIDVC.h"

@interface MonBeaconTrackerTVC : UITableViewController <CLLocationManagerDelegate, UUIDUpdateProtocol>

@end
