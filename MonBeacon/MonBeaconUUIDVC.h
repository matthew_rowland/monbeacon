//
//  MonBeaconUUIDVC.h
//  MonBeacon
//
//  Created by Matthew Rowland on 4/14/14.
//  Copyright (c) 2014 Matthew Rowland. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreBluetooth/CoreBluetooth.h>

@protocol UUIDUpdateProtocol <NSObject>

@required
- (void)UUIDToTrack: (NSString *)UUIDFromData;
@end

@interface MonBeaconUUIDVC : UIViewController <UITextFieldDelegate,CBPeripheralManagerDelegate>

@property (strong, nonatomic) id<UUIDUpdateProtocol> delegate;

@end
