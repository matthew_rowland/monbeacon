//
//  MonBeaconUUIDVC.m
//  MonBeacon
//
//  Created by Matthew Rowland on 4/14/14.
//  Copyright (c) 2014 Matthew Rowland. All rights reserved.
//

#import "MonBeaconUUIDVC.h"

@interface MonBeaconUUIDVC ()
@property (weak, nonatomic) IBOutlet UITextField *broadcastedUUID;
@property (strong, nonatomic) IBOutlet UITextField *trackerUUID;
@property (weak, nonatomic) IBOutlet UILabel *broadcastingLabel;
@property (weak, nonatomic) IBOutlet UITextField *major;
@property (weak, nonatomic) IBOutlet UITextField *minor;
@property (strong, nonatomic) CLBeaconRegion *myBeaconRegion;
@property (strong, nonatomic) NSDictionary *myBeaconData;
@property (strong, nonatomic) CBPeripheralManager *peripheralManager;
@end

@implementation MonBeaconUUIDVC

- (void)TransmitBeacon {
    NSUUID *MonBeaconUUID = [[NSUUID alloc] initWithUUIDString:self.broadcastedUUID.text];
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    
    // Initialize the Beacon Region
    self.myBeaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:MonBeaconUUID major:[[formatter numberFromString:self.major.text] unsignedShortValue] minor:[[formatter numberFromString:self.minor.text] unsignedShortValue] identifier:@"com.monsanto"];
    self.myBeaconData = [self.myBeaconRegion peripheralDataWithMeasuredPower:nil];
    self.peripheralManager = [[CBPeripheralManager alloc] initWithDelegate:self queue:nil options:nil];
    
}

- (void)peripheralManagerDidUpdateState:(CBPeripheralManager*)peripheral
{
    if (peripheral.state == CBPeripheralManagerStatePoweredOn)
    {
        // Bluetooth is on
        // Start broadcasting
        [self.peripheralManager startAdvertising:self.myBeaconData];
    }
    else if (peripheral.state == CBPeripheralManagerStatePoweredOff)
    {
        // Bluetooth isn't on. Stop broadcasting
        [self.peripheralManager stopAdvertising];
    }
    else if (peripheral.state == CBPeripheralManagerStateUnsupported)
    {
        //Unsupported
    }
}

- (IBAction)stopBroadcast:(UIButton *)sender {
    self.broadcastingLabel.text = @"None";
    self.peripheralManager = nil;
}

- (IBAction)updateBroadcastedUUID:(UIButton *)sender {
    [self.broadcastedUUID resignFirstResponder];
    [self.major resignFirstResponder];
    [self.minor resignFirstResponder];
}

- (IBAction)updateTrackerUUID:(UIButton *)sender {
    [self.trackerUUID resignFirstResponder];
    [self.delegate UUIDToTrack:self.trackerUUID.text];
}

- (IBAction)broadcastBeacon:(UIButton *)sender {
    //what if it's blank?
    self.broadcastingLabel.text = self.broadcastedUUID.text;
    [self TransmitBeacon];
    
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.broadcastedUUID setDelegate:self];
    [self.trackerUUID setDelegate:self];
    [self.major setDelegate:self];
    [self.minor setDelegate:self];
}

- (void)viewWillDisappear:(BOOL)animated {
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
