//
//  MonBeaconAppDelegate.h
//  MonBeacon
//
//  Created by Matthew Rowland on 4/10/14.
//  Copyright (c) 2014 Matthew Rowland. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MonBeaconAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
