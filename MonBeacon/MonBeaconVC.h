//
//  MonBeaconVC.h
//  MonBeacon
//
//  Created by Matthew Rowland on 4/16/14.
//  Copyright (c) 2014 Matthew Rowland. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface MonBeaconVC : UIViewController <CLLocationManagerDelegate>
@end
