//
//  MonBeaconTests.m
//  MonBeaconTests
//
//  Created by Matthew Rowland on 4/10/14.
//  Copyright (c) 2014 Matthew Rowland. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface MonBeaconTests : XCTestCase

@end

@implementation MonBeaconTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
